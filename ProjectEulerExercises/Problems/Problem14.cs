﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectEulerExercises.Problems {
	/// <summary>
	/// The following iterative sequence is defined for the set of positive integers:
	/// 
	/// n → n/2 (n is even)
	/// n → 3n + 1 (n is odd)
	/// 
	/// Using the rule above and starting with 13, we generate the following sequence:
	/// 
	/// 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
	/// It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
	/// 
	/// Which starting number, under one million, produces the longest chain?
	/// 
	/// NOTE: Once the chain starts the terms are allowed to go above one million.
	/// </summary>
	public class Problem14 : Problem {
		/// <summary>
		/// Returns the number that produces the longest Collatz chain under the given number.
		/// </summary>
		/// <returns>The number that produces the longest Collatz chain under the given number.</returns>
		public static long Method1(long under = 1000000) {
            List<Task<List<long>>> tasks = new List<Task<List<long>>>();

            for(int i = 1; i < under; i++) {
                int temp = i;
                tasks.Add(Task<List<long>>.Run(() => Collatz(new List<long> { i })));
            }

			var completedTasks = Task.WhenAll<List<long>>(tasks).Result;

			long count = completedTasks.Max(l => l.Count);

			return completedTasks.Where(l => l.Count == count).First()[0];
        }

		/// <summary>
		/// PLINQ implementation of the same method above. Runs consistently faster.
		/// </summary>
		/// <param name="under"></param>
		/// <returns></returns>
		public static long Method2(int under = 1000000) {
			var numbers = Enumerable.Range(1, --under);

			var query =
				from num in numbers.AsParallel()
				select num;

			var concurrentBag = new ConcurrentBag<List<long>>();

			query.ForAll(n => concurrentBag.Add(Collatz(new List<long> { n })));

			long count = concurrentBag.Max(l => l.Count);

			return concurrentBag.Where(l => l.Count == count).First()[0];
		}

		/// <summary>
		/// Less memory hungry solution because I'm not keeping the entire chain.
		/// </summary>
		/// <param name="under"></param>
		/// <returns></returns>
		public static long Method3(int under = 1000000) {
			var numbers = Enumerable.Range(1, --under);

			var query =
				from num in numbers.AsParallel()
				select num;

			var concurrentBag = new ConcurrentBag<ValueTuple<long, long, long>>();

			query.ForAll(n => concurrentBag.Add(CollatzLength(( n, 1, n ))));

			long count = concurrentBag.Max(l => l.Item2);

			return concurrentBag.Where(l => l.Item2 == count).First().Item3;
		}

		/// <summary>
		/// Recursive collatz chain computing.
		/// </summary>
		/// <param name="numberList"></param>
		/// <returns>List of the collatz chain values.</returns>
		private static List<long> Collatz(List<long> numberList) {
			long current = numberList[numberList.Count - 1];

			if(current == 1) {
				numberList.Add(1);
				return numberList;
            }

			if(current % 2 == 0) {
				numberList.Add(current / 2);
				Collatz(numberList);
            }
            else {
				numberList.Add(current * 3 + 1);
				Collatz(numberList);
            }

			return numberList;
        }

		/// <summary>
		/// Less memory hungry collatz method.
		/// Just keeps the length so far and current number.
		/// </summary>
		/// <returns></returns>
		private static (long, long, long) CollatzLength((long, long, long) tuple) {
			if (tuple.Item1 == 1) {
				tuple.Item2++;
				return tuple;
			}

			if (tuple.Item1 % 2 == 0) {
				tuple.Item1 /= 2;
				tuple.Item2++;
				CollatzLength(tuple);
			}
			else {
				tuple.Item1 = tuple.Item1 * 3 +1;
				tuple.Item2++;
				CollatzLength(tuple);
			}

			return tuple;
		}
    }
}
