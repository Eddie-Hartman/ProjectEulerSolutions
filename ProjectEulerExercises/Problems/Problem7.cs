﻿namespace ProjectEulerExercises.Problems
{
    /// <summary>
    /// By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
    ///
    /// What is the 10 001st prime number?
    /// </summary>
    public class Problem7 : Problem {
        /// <summary>
        /// Finds the nth prime number.
        /// </summary>
        /// <param name="number">The nth prime to find.</param>
        /// <returns></returns>
        public static long Method1(int number = 10001) {
            int primes = 0;
            long current = 0;
            while(primes < number)
            {
                current++;
                if (Utils.IsPrimeSlow(current))
                {
                    primes++;
                }
            }

            return current;
        }
    }
}
