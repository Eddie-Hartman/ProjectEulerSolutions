﻿using System.Collections.Generic;

namespace ProjectEulerExercises.Problems {
	/// <summary>
	/// You are given the following information, but you may prefer to do some research for yourself.
	/// 
	/// 1 Jan 1900 was a Monday.
	/// Thirty days has September,
	/// April, June and November.
	/// All the rest have thirty-one,
	/// Saving February alone,
	/// Which has twenty-eight, rain or shine.
	/// And on leap years, twenty-nine.
	/// A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
	/// How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
	/// </summary>
	public class Problem19 : Problem {

		static readonly List<int> thirtyDayList = new List<int> { 8, 3, 5, 10 };

		/// <summary>
		/// Returns how many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000).
		/// </summary>
		/// <returns></returns>
		public static long Method1() {
			int dayOfWeek = 2;
			int year = 1901;
			int month = 0;
			int day = 0;
			bool leapYear = false;

			int count = 0;

			while(year < 2001) {
				if(dayOfWeek == 0 && day == 0) {
					count++;
                }

				dayOfWeek = NextDay(dayOfWeek);

				if(EndOfMonth(month, day, leapYear)) {
					if (month == 11) {
						year++;
						leapYear = IsLeapYear(year);
					}
					month = NextMonth(month);
					day = 0;
                }
                else {
					day++;
                }

            }

			return count;
		}

		private static int NextDay(int day) {
			if (day == 6)
				return 0;
			return ++day;
        }

		private static int NextMonth(int month) {
			if (month == 11)
				return 0;
			return ++month;
        }

		private static bool EndOfMonth(int month, int day, bool leapYear) {
			if(month == 1) {
                if (leapYear) {
					return day == 28;
                }
				return day == 27;
            }

			if (thirtyDayList.Contains(month)) {
				return day == 29;
			}

			return day == 30;
        }

		/// <summary>
		/// Given a year, determines if it is a leap year.
		/// </summary>
		/// <param name="year">The year to determine.</param>
		/// <returns>True if the year is a leap year.</returns>
		private static bool IsLeapYear(int year) {
			if (year % 4 != 0)
				return false;
			if (year % 100 == 0 && year % 400 != 0)
				return false;
			return true;
        }
	}
}
