﻿using System;
using System.Text;

namespace ProjectEulerExercises.Problems {
	/// <summary>
	/// 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
	/// 
	/// What is the sum of the digits of the number 2^1000?
	/// </summary>
	public class Problem16 : Problem {
		/// <summary>
		/// Raises 2 to the given exponent and then adds each individual digit together.
		/// </summary>
		/// <returns></returns>
		public static long Method1(long exponent = 1000) {
			StringBuilder stringBuilder = new StringBuilder("2");

			for(int i = 2; i <= exponent; i++) {
				SquareString(stringBuilder);
            }

			long sum = 0;
			
			for(int i = 0; i < stringBuilder.Length; i++) {
				sum += (int)Char.GetNumericValue(stringBuilder[i]);
            }

			return sum;
        }

		/// <summary>
		/// Squares the given stringBuilder representation of a number.
		/// </summary>
		/// <param name="stringBuilder"></param>
		/// <returns></returns>
		private static StringBuilder SquareString(StringBuilder stringBuilder) {
			bool carry = false;
			for(int i = stringBuilder.Length - 1; i >= 0; i--) {
				int number = (int)Char.GetNumericValue(stringBuilder[i]);
				number += number + (carry ? 1 : 0);
				carry = number > 9;
				number = number % 10;
				stringBuilder[i] = (char)(number + 48);
            }
            if (carry) {
				stringBuilder.Insert(0, 1);
            }
			return stringBuilder;
        }
	}
}
