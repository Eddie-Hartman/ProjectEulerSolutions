﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectEulerExercises.Problems {
	/// <summary>
	/// Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.
	/// 
	/// How many such routes are there through a 20×20 grid?
	/// </summary>
	public class Problem15 : Problem {
		/// <summary>
		/// Returns the number of paths from top left to bottom right
		/// using only right and down options in an N by N grid.
		/// </summary>
		/// <returns></returns>
		public static long Method1(long grid = 20) {
			return Paths(grid, grid);
        }

		/// <summary>
		/// Dynamic programming version of solution.
		/// </summary>
		/// <param name="grid"></param>
		/// <returns></returns>
		public static long Method2(long grid = 20) {
			Dictionary<(long, long), long> gridValues = new Dictionary<(long, long), long>();

			return PathsDynamic(grid, grid, gridValues);
		}

		/// <summary>
		/// Same dynamic programming solution but with array instead.
		/// </summary>
		/// <param name="grid"></param>
		/// <returns></returns>
		public static long Method3(long grid = 20) {
			long[,] gridValues = new long[grid + 1, grid + 1];
			gridValues[0, 0] = 1;

			return PathsDynamicArray(grid, grid, gridValues);
        }

		private static long Paths(long x, long y) {
			long total = 0;
			if(x == 0 || y == 0) {
				return 1;
            }
			total += Paths(x - 1, y);
			total += Paths(x, --y);
			return total;
        }

		private static long PathsDynamic(long x, long y, Dictionary<(long, long), long> gridValues) {
			long total = 0;
			if(gridValues.ContainsKey((x,y))) {
				return gridValues[(x,y)];
            }
			if(x == 0 || y == 0) {
				return 1;
            }
			if (!gridValues.ContainsKey((x - 1, y))) {
				gridValues.Add((x - 1, y), PathsDynamic(x - 1, y, gridValues));
			}
			total += gridValues[(x - 1, y)];
			if (!gridValues.ContainsKey((x, y - 1))) {
				gridValues.Add((x, y - 1), PathsDynamic(x, y - 1, gridValues));
			}
			total += gridValues[(x, y - 1)];
			return total;
		}

		private static long PathsDynamicArray(long x, long y, long[,] gridValues) {
			long total = 0;
			if (gridValues[x,y] != 0) {
				return gridValues[x, y];
			}
			if (x == 0 || y == 0) {
				return 1;
			}
			if (gridValues[x - 1, y] == 0) {
				gridValues[x - 1, y] = PathsDynamicArray(x - 1, y, gridValues);
			}
			total += gridValues[x - 1, y];
			if (gridValues[x, y - 1] == 0) {
				gridValues[x, y - 1] = PathsDynamicArray(x, y - 1, gridValues);
			}
			total += gridValues[x, y - 1];
			return total;
		}
	}
}
