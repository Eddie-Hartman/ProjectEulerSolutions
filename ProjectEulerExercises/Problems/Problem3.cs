﻿namespace ProjectEulerExercises.Problems {
    /// <summary>
    /// The prime factors of 13195 are 5, 7, 13 and 29.
    ///
    /// What is the largest prime factor of the number 600851475143 ?
    /// </summary>
    public class Problem3 : Problem {
        /// <summary>
        /// Finds the largest prime factor of the given number.
        /// </summary>
        /// <param name="number">The number to go to.</param>
        /// <returns></returns>
        public static long Method1(long number = 600851475143) {
            for (long i = 1; i <= number; i++) {
                if (number % i == 0) {
                    long current = number / i;
                    if (Utils.IsPrimeSlow(current))
                        return current;
                }
            }
            return 0;
        }
    }
}
