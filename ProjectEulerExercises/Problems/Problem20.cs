﻿using System;
using System.Numerics;

namespace ProjectEulerExercises.Problems {
	/// <summary>
	/// n! means n × (n − 1) × ... × 3 × 2 × 1
	/// 
	/// For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
	/// and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
	/// 
	/// Find the sum of the digits in the number 100!
	/// </summary>
	public class Problem20 : Problem {

		/// <summary>
		/// Find the sum of digits of the factorial of the number given.
		/// </summary>
		/// <returns></returns>
		public static long Method1(int factorial = 100)
		{
			BigInteger current = new BigInteger(factorial);

			for(int i = factorial - 1; i > 1; i--) {
				current *= i;
            }

			return SumStringDigits(current.ToString());
		}

		private static long SumStringDigits(string stringBuilder) {
			long value = 0;
			for(int i = 0; i < stringBuilder.Length; i++) {
				int number = (int)Char.GetNumericValue(stringBuilder[i]);
				value += number;
			}

			return value;
        }
	}
}
