﻿using System;
using System.Text;

namespace ProjectEulerExercises.Problems {
	/// <summary>
	/// If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
	/// 
	/// If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
	/// 
	/// NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters.
	/// The use of "and" when writing out numbers is in compliance with British usage.
	/// </summary>
	public class Problem17 : Problem {
		/// <summary>
		/// Finds the number of letters used to write out the numbers 1 to 1000.
		/// </summary>
		/// <remarks>
		/// I did not want to make a more general solution to this problem.
		/// 
		/// I also know that simply counting letters would be more efficient.
		/// This could be a potential method2.
		/// </remarks>
		/// <returns></returns>
		public static long Method1() {

			StringBuilder stringBuilder = new StringBuilder();

			for(int i = 1; i < 1000; i++) {
				int hundreds = i / 100;
                switch (hundreds) {
					case 9:
						stringBuilder.Append("nine hundred");
						break;
					case 8:
						stringBuilder.Append("eight hundred");
						break;
					case 7:
						stringBuilder.Append("seven hundred");
						break;
					case 6:
						stringBuilder.Append("six hundred");
						break;
					case 5:
						stringBuilder.Append("five hundred");
						break;
					case 4:
						stringBuilder.Append("four hundred");
						break;
					case 3:
						stringBuilder.Append("three hundred");
						break;
					case 2:
						stringBuilder.Append("two hundred");
						break;
					case 1:
						stringBuilder.Append("one hundred");
						break;
				}

				if(i > 100 && i > hundreds * 100) {
					stringBuilder.Append("and");
                }

				int remainder = i % 100;

				if(remainder > 9 && remainder < 20) {
					switch (remainder) {
						case 10:
							stringBuilder.Append("ten");
							break;
						case 11:
							stringBuilder.Append("eleven");
							break;
						case 12:
							stringBuilder.Append("twelve");
							break;
						case 13:
							stringBuilder.Append("thirteen");
							break;
						case 14:
							stringBuilder.Append("fourteen");
							break;
						case 15:
							stringBuilder.Append("fifteen");
							break;
						case 16:
							stringBuilder.Append("sixteen");
							break;
						case 17:
							stringBuilder.Append("seventeen");
							break;
						case 18:
							stringBuilder.Append("eighteen");
							break;
						case 19:
							stringBuilder.Append("nineteen");
							break;
					}
				}
                else {
					int tens = remainder / 10;
					switch (tens) {
						case 2:
							stringBuilder.Append("twenty");
							break;
						case 3:
							stringBuilder.Append("thirty");
							break;
						case 4:
							stringBuilder.Append("forty");
							break;
						case 5:
							stringBuilder.Append("fifty");
							break;
						case 6:
							stringBuilder.Append("sixty");
							break;
						case 7:
							stringBuilder.Append("seventy");
							break;
						case 8:
							stringBuilder.Append("eighty");
							break;
						case 9:
							stringBuilder.Append("ninety");
							break;
					}
				}
				if(remainder < 10 || remainder > 20) {
					AppendLastDigit(stringBuilder, remainder);
                }
            }

			stringBuilder.Append("one thousand");

			stringBuilder.Replace(" ", "");
			stringBuilder.Replace("-", "");

			return stringBuilder.Length;
        }

		/// <summary>
		/// Adds the last digit to the given string builder
		/// </summary>
		/// <param name="builder"></param>
		/// <param name="number"></param>
		private static void AppendLastDigit(StringBuilder builder, int number) {
			int digit = number % 10;
            switch (digit) {
				case 1:
					builder.Append("one");
					break;
				case 2:
					builder.Append("two");
					break;
				case 3:
					builder.Append("three");
					break;
				case 4:
					builder.Append("four");
					break;
				case 5:
					builder.Append("five");
					break;
				case 6:
					builder.Append("six");
					break;
				case 7:
					builder.Append("seven");
					break;
				case 8:
					builder.Append("eight");
					break;
				case 9:
					builder.Append("nine");
					break;
			}
        }
	}
}
