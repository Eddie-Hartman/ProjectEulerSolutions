﻿namespace ProjectEulerExercises.Problems {
    /// <summary>
    /// The sum of the squares of the first ten natural numbers is,
    /// 1^2 + 2^2 + 3^3 ... 10^2 = 385
    /// The square of the sum of the first ten natural numbers is,
    /// (1 + 2 + 3 + ... 10)^2 = 55^2 = 3025
    /// Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is .
    /// 3025 - 385 = 2640
    /// Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
    /// </summary>
    public class Problem6 : Problem {
        /// <summary>
        /// Finds the difference between the sum of squares and the square of the sum of the numbers from 1 to n.
        /// </summary>
        /// <param name="max">The max number to go to.</param>
        /// <returns></returns>
        public static long Method1(int max = 100) {
            long sumOfSquares = 0;
            long squareOfSum = 0;
            for (int i = 1; i <= max; i++) {
                sumOfSquares += i * i;
                squareOfSum += i;
            }

            squareOfSum *= squareOfSum;

            return squareOfSum - sumOfSquares;
        }
    }
}
