﻿using System;

namespace ProjectEulerExercises.Problems {
    /// <summary>
    /// 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
    ///
    /// What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
    /// </summary>
    public class Problem5 : Problem {
        /// <summary>
        /// Finds the smallest positive number that is evenly divisible by all of the numbers from 1 to n.
        /// </summary>
        /// <param name="max">The number of digits.</param>
        /// <returns></returns>
        public static long Method1(int max = 20) {
            int largestPrime = 1;
            for (int i = max; i > 1; i--) {
                if (Utils.IsPrimeSlow(i)) {
                    largestPrime = i;
                    break;
                }
            }

            int loop = 1;
            while (loop < Int32.MaxValue) {
                int current = loop * largestPrime;

                bool evenlyDivisible = true;
                for (int i = 2; i <= max; i++) {
                    if (current % i != 0) {
                        evenlyDivisible = false;
                        break;
                    }
                }

                if (evenlyDivisible)
                    return current;

                loop++;
            }

            return 0;
        }
    }
}
