﻿namespace ProjectEulerExercises.Problems {
    /// <summary>
    /// If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
    ///
    /// Find the sum of all the multiples of 3 or 5 below 1000.
    /// </summary>
    public class Problem1 : Problem {

        /// <summary>
        /// Finds all multiples of 3 or 5 up to the given number and returns their sum.
        /// </summary>
        /// <param name="number">The number to find multiples up to.</param>
        /// <returns></returns>
        public static int Method1(int number = 1000) {
            int total = 0;
            for (int i = 0; i < number; i++) {
                if (i % 3 == 0) {
                    total += i;
                }
                else if (i % 5 == 0) {
                    total += i;
                }
            }
            return total;
        }
    }
}
