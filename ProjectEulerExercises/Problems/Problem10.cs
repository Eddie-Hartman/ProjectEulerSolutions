﻿namespace ProjectEulerExercises.Problems {
    /// <summary>
    /// The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
    ///
    /// Find the sum of all the primes below two million.
    /// </summary>
    public class Problem10 : Problem {

        //This is the original method I used to solve the problem.
        //Unfortunately, it is too slow and I do not want it to slow down running all solutions.
        /// <summary>
        /// Finds the sum of all prime numbers from 0 to the given number.
        /// </summary>
        /// <param name="number">The number to find the summation of all primes up to.</param>
        /// <returns></returns>
        //public static double Method2(int number = 2000000) {
        //    long sum = 0;

        //    for(int i = 1; i < number; i++)
        //    {
        //        if (Utils.IsPrimeSlow(i)) {
        //            sum += i;
        //        }
        //    }

        //    return sum;
        //}

        /// <summary>
        /// Same as Method1 but with a faster prime method.
        /// </summary>
        /// <param name="number">The number to find the summation of all primes up to.</param>
        /// <returns></returns>
        public static double Method1(int number = 2000000) {
            long sum = 0;

            for (int i = 1; i < number; i++) {
                if (Utils.IsPrime(i)) {
                    sum += i;
                }
            }

            return sum;
        }
    }
}
