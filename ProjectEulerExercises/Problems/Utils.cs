﻿using System;

namespace ProjectEulerExercises.Problems
{
    /// <summary>
    /// Utilities class full of helper methods.
    /// </summary>
    class Utils
    {
        [Obsolete("This is my original method for finding prime and should no longer be used.")]
        /// <summary>
        /// Checks if a number is prime.
        /// </summary>
        /// <param name="number">The number to check if it's prime.</param>
        /// <returns></returns>
        public static bool IsPrimeSlow(long number)
        {
            if (number == 1)
                return false;
            long half = (long)System.Math.Ceiling(number / 2.0);

            for (long i = 2; i <= half; i++)
            {
                if (number % i == 0)
                    return false;
            }
            return true;
        }

        /// <summary>
        /// This is the faster prime method after researching a faster algorithm.
        /// 
        /// Reference - <see href="https://en.wikipedia.org/wiki/Primality_test#C#_code"/>
        /// </summary>
        /// <param name="number">The number to check if it's prime.</param>
        /// <returns>True if prime false if not.</returns>
        public static bool IsPrime(long number) {
            if (number < 2)
                return false;
            if (number < 4 || number % 2 == 0 || number % 3 == 0)
                return true;

            long limit = (long)Math.Ceiling(Math.Sqrt(number));
            for(long i = 5; i < limit; i += 6) {
                if((number % i) == 0 || number % (i + 2) == 0) {
                    return false;
                }
            }

            return true;
        }
    }
}
