﻿namespace ProjectEulerExercises.Problems {
    /// <summary>
    /// A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
    ///
    /// Find the largest palindrome made from the product of two 3-digit numbers.
    /// </summary>
    public class Problem4 : Problem {
        /// <summary>
        /// Finds the largest palindrome made from the product of two n-digit numbers.
        /// </summary>
        /// <param name="numberOfDigits">The number of digits.</param>
        /// <returns></returns>
        public static long Method1(int numberOfDigits = 3) {
            int highest = 1;
            for (int i = 1; i < numberOfDigits; i++) {
                highest = highest * 10 + 1;
            }
            highest *= 9;

            //The largest palindrome we've found so far.
            int palindrome = 0;
            for (int i = highest; i > 1; i--) {
                for (int k = highest; k > 1; k--) {
                    int current = i * k;
                    if (current > palindrome) {
                        if (IsPalindrome(current))
                            palindrome = current;
                    }
                }
            }
            return palindrome;
        }

        /// <summary>
        /// Checks if a number is a palindrome.
        /// </summary>
        /// <param name="number">The number to check if it's a palindrome.</param>
        /// <returns></returns>
        private static bool IsPalindrome(int number) {
            string num = number.ToString();
            int length = num.Length;
            for (int i = 0; i < length / 2; i++) {
                if (num[i] != num[num.Length - i - 1])
                    return false;
            }
            return true;
        }
    }
}
