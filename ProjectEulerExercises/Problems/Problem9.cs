﻿using System;

namespace ProjectEulerExercises.Problems
{
    /// <summary>
    /// A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
    ///
    /// a2 + b2 = c2
    /// For example, 32 + 42 = 9 + 16 = 25 = 52.
    /// 
    /// There exists exactly one Pythagorean triplet for which a + b + c = 1000.
    /// Find the product abc.
    /// </summary>
    public class Problem9 : Problem {
        /// <summary>
        /// Finds the product of a pythagorean triplet where a + b + c equals the given number.
        /// </summary>
        /// <param name="number">The number to find a pythagorean triplet for.</param>
        /// <returns></returns>
        public static double Method1(int number = 1000) {
            int a, b;
            double c;
            double current;

            for(int i = 1; i < number; i++)
            {
                a = i;
                for(int k = i; k < number; k++)
                {
                    b = k;
                    c = a * a + b * b;
                    c = Math.Sqrt(c);

                    current = a + b + c;

                    if(current == number)
                    {
                        return a * b * c;
                    }
                    else if(current > number)
                    {
                        break;
                    }
                }
            }

            return 0;
        }
    }
}
