﻿using System.IO;
using System.Linq;

namespace ProjectEulerExercises.Problems {
	/// <summary>
	/// By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
	/// 
	/// >3
	/// >7 4
	/// 2 >4 6
	/// 8 5 >9 3
	/// 
	/// That is, 3 + 7 + 4 + 9 = 23.
	/// 
	/// Find the maximum total from top to bottom of the triangle below:
	/// 
	/// 75
	/// 95 64
	/// 17 47 82
	/// 18 35 87 10
	/// 20 04 82 47 65
	/// 19 01 23 75 03 34
	/// 88 02 77 73 07 63 67
	/// 99 65 04 28 06 16 70 92
	/// 41 41 26 56 83 40 80 70 33
	/// 41 48 72 33 47 32 37 16 94 29
	/// 53 71 44 65 25 43 91 52 97 51 14
	/// 70 11 33 28 77 73 17 78 39 68 17 57
	/// 91 71 52 38 17 14 91 43 58 50 27 29 48
	/// 63 66 04 68 89 53 67 30 73 16 69 87 40 31
	/// 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
	/// 
	/// NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)
	/// </summary>
	public class Problem18 : Problem {

		/// <summary>
		/// Finds the maximum total from top to bottom for the triangle above via brute force.
		/// </summary>
		/// <returns></returns>
		public static long Method1() {
			int[][] triangle = new int[15][];
			InitializeTriangle(triangle);

			return FindHighest(triangle, 0, 0, 0, 0);
		}

		/// <summary>
		/// Finds the maximum total from top to bottom for the triangle above
		/// using memoization to shortcut paths.
		/// </summary>
		/// <returns></returns>
		public static long Method2() {
			int[][] triangle = new int[15][];
			long[][] highestMemoized = new long[15][];

			InitializeTriangle(triangle);
			InitializeHighest(highestMemoized);

			return FindHighestMemoized(triangle, 0, 0, 0, 0, highestMemoized);
        }

		/// <summary>
		/// Recursive algorithm for finding the highest path in the traingle.
		/// </summary>
		/// <param name="triangle">The jagged array of the triangle.</param>
		/// <param name="y">Depth so far.</param>
		/// <param name="x">Look at the xth number in array depth y.</param>
		/// <param name="currentTotal">The total so far.</param>
		/// <param name="highestSoFar">The highest path found so far.</param>
		/// <returns></returns>
		private static long FindHighest(int[][] triangle, int y, int x, long currentTotal, long highestSoFar) {
			currentTotal += triangle[y][x];
			if (currentTotal > highestSoFar)
				highestSoFar = currentTotal;

			y++;
			if (y >= triangle.Length)
				return highestSoFar;

			long highestNested;
			highestNested = FindHighest(triangle, y, x, currentTotal, highestSoFar);
			if (highestNested > highestSoFar)
				highestSoFar = highestNested;
			highestNested = FindHighest(triangle, y, x + 1, currentTotal, highestSoFar);
			if (highestNested > highestSoFar)
				highestSoFar = highestNested;

			return highestSoFar;
		}

		/// <summary>
		/// Recursive algorithm for finding the highest path in the traingle.
		/// </summary>
		/// <param name="triangle">The jagged array of the triangle.</param>
		/// <param name="y">Depth so far.</param>
		/// <param name="x">Look at the xth number in array depth y.</param>
		/// <param name="currentTotal">The total so far.</param>
		/// <param name="highestSoFar">The highest path found so far.</param>
		/// <param name="highestMemoized">Memoization of highest possible at this path.</param>
		/// <returns></returns>
		private static long FindHighestMemoized(int[][] triangle, int y, int x, long currentTotal, long highestSoFar, long[][] highestMemoized) {
			currentTotal += triangle[y][x];
			if(currentTotal < highestMemoized[y][x]) {
				return currentTotal;
            }
            else {
				highestMemoized[y][x] = currentTotal;
            }
			if (currentTotal > highestSoFar)
				highestSoFar = currentTotal;

			y++;
			if (y >= triangle.Length)
				return highestSoFar;

			long highestNested;
			highestNested = FindHighestMemoized(triangle, y, x, currentTotal, highestSoFar, highestMemoized);
			if (highestNested > highestSoFar)
				highestSoFar = highestNested;
			highestNested = FindHighestMemoized(triangle, y, x + 1, currentTotal, highestSoFar, highestMemoized);
			if (highestNested > highestSoFar)
				highestSoFar = highestNested;

			return highestSoFar;
		}

		private static void InitializeTriangle(int[][] triangle) {
			string triangleString = @"75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23";

			triangleString = triangleString.Replace(' ', ',');
			StringReader reader = new StringReader(triangleString);
			string currentLine;
			int line = 0;
			while((currentLine = reader.ReadLine()) != null) {
				triangle[line] = currentLine.Split(',').Select(n => int.Parse(n)).ToArray();
				line++;
            }

		}

		private static void InitializeHighest(long[][] highestMemoized) {
			for(int i = 0; i < 15; i++) {
				highestMemoized[i] = new long[i+1];
            }
        }
	}
}
