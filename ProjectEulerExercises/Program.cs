﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectEulerExercises {
    /// <summary>
    /// Runs every Problem and Method and displays the solutions they come up with.
    /// 
    /// Problems are from https://projecteuler.net/
    /// </summary>
    class Program {
        static void Main(string[] args) {
            IEnumerable<Problem> problems = typeof(Problem)
                .Assembly.GetTypes()
                .Where(t => t.IsSubclassOf(typeof(Problem)) && !t.IsAbstract)
                .Select(t => (Problem)Activator.CreateInstance(t));

            //Sort by class name but take away "Problem" so that it is ordered by numbers.
            problems = problems.OrderBy(m => Int32.Parse(m.GetType().Name.Substring(7)));

            #region Argument for specific problem and method passed.
            int methodParam = 0;
            if (args.Length > 0) {
                if (args.Length == 1 || args.Length == 2) {
                    if (Int32.TryParse(args[0], out int problem)) {
                        problems = problems.Where(m => m.GetType().Name == "Problem" + problem);
                        if (args.Length > 1) {
                            if (Int32.TryParse(args[1], out methodParam)) {

                            }
                            else {
                                throw new ArgumentException();
                            }
                        }
                    }
                    else {
                        throw new ArgumentException();
                    }
                }
                else {
                    throw new ArgumentException();
                }
            }
            #endregion

            StringBuilder stringBuilder = new StringBuilder();
            if (methodParam != 0) {
                var type = problems.ElementAt(0).GetType();
                var method = type.GetMethod("Method" + methodParam);
                int paramNumber = method.GetParameters().Length;
                object[] parameters = paramNumber == 0 ? null : new object[paramNumber];

                for (int p = 0; p < paramNumber; p++) {
                    parameters[p] = Type.Missing;
                }

                stringBuilder.AppendLine(type.Name + ": " + method.Name + ": " + method.Invoke(null, parameters).ToString());
            }
            else {
                foreach (Problem problem in problems) {
                    var type = problem.GetType();
                    int methodCount = type.GetMethods(System.Reflection.BindingFlags.DeclaredOnly | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public).Length;

                    for (int i = 1; i <= methodCount; i++) {
                        var method = type.GetMethod("Method" + i);
                        int paramNumber = method.GetParameters().Length;
                        object[] parameters = paramNumber == 0 ? null : new object[paramNumber];

                        for (int p = 0; p < paramNumber; p++) {
                            parameters[p] = Type.Missing;
                        }

                        stringBuilder.AppendLine(type.Name + ": " + method.Name + ": " + method.Invoke(null, parameters).ToString());
                    }
                }
            }
            Console.WriteLine(stringBuilder.ToString());
        }
    }
}
