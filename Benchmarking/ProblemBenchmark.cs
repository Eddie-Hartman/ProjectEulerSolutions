﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Exporters.Csv;
using ProjectEulerExercises.Problems;

namespace Benchmarking {

    [MemoryDiagnoser]
    [Config(typeof(Config))]
    [RPlotExporter]
    public class Problem10Benchmark {
        [Benchmark(Baseline = true)]
        public void Benchmark1() {
            Problem10.Method1();
        }

        //[Benchmark]
        //public void Benchmark2() {
        //    Problem10.Method2();
        //}
    }

    [MemoryDiagnoser]
    [Config(typeof(Config))]
    [RPlotExporter]
    public class Problem14Benchmark {
        [Benchmark(Baseline = true)]
        public void Benchmark1() {
            Problem14.Method1();
        }

        [Benchmark]
        public void Benchmark2() {
            Problem14.Method2();
        }

        [Benchmark]
        public void Benchmark3() {
            Problem14.Method3();
        }
    }

    [MemoryDiagnoser]
    [Config(typeof(Config))]
    [RPlotExporter]
    public class Problem15Benchmark {
        [Benchmark(Baseline = true)]
        //public void Benchmark1() {
        //    Problem15.Method1();
        //}

        //[Benchmark]
        public void Benchmark2() {
            Problem15.Method2();
        }

        [Benchmark]
        public void Benchmark3() {
            Problem15.Method3();
        }
    }



    [MemoryDiagnoser]
    [Config(typeof(Config))]
    [RPlotExporter]
    public class Problem18Benchmark {
        [Benchmark(Baseline = true)]
        public void Benchmark1() {
            Problem18.Method1();
        }

        [Benchmark]
        public void Benchmark2() {
            Problem18.Method2();
        }
    }

    public class Config : ManualConfig {
        public Config() {
            AddExporter(CsvMeasurementsExporter.Default);
            AddExporter(RPlotExporter.Default);
        }
    }
}
