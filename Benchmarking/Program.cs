﻿using BenchmarkDotNet.Running;

namespace Benchmarking {
    /// <summary>
    /// This project will eventually be used to compare runtimes and memory usage of solution methods for problems.
    /// </summary>
    class Program {
        static void Main(string[] args) {
            BenchmarkRunner.Run<Problem18Benchmark>();
        }
    }
}
